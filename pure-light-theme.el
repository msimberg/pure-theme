(require 'pure-common)
(deftheme pure-light "pure-theme light")
(create-pure-theme 'light 'pure-light)
(provide-theme 'pure-light)
