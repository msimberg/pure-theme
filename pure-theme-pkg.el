(define-package
  "pure-theme"
  "0.1"
  "Simple black-on-white and white-on-black themes, with colors used sparsely for highlighting"
  'nil
  :url "https://gitlab.com/msimberg/pure-theme"
  :keywords '("color" "theme"))

