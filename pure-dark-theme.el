(require 'pure-common)
(deftheme pure-dark "pure-theme dark")
(create-pure-theme 'dark 'pure-dark)
(provide-theme 'pure-dark)
