;;; pure-theme-common.el --- Simple black-on-white and white-on-black themes,
;;; with colors used sparingly for highlighting

;; Author: Mikael Simberg
;;
;; Version: 0.1
;; Keywords: color, theme
;; Package-Requires: ((emacs "24"))

;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <http://www.gnu.org/licenses/>.

;; This file is not part of Emacs.

;;; Commentary:

;; Based on the excellent spacemacs-theme
;; (<https://github.com/nashamri/spacemacs-theme>). Colors are based on Google's
;; Material Design color palettes
;; (<https://material.io/guidelines/style/color.html#color-color-tool>).

;;; Code:

(require 'material-colors)

(defmacro dyn-let (varlist fn setfaces setvars)
  (list 'let (append varlist (funcall fn)) setfaces setvars))

(defgroup pure-theme nil
  "pure-theme options."
  :group 'faces)

(defcustom pure-theme-comment-bg nil
  "Use a background for comment lines."
  :type 'boolean
  :group 'pure-theme)

(defcustom pure-theme-org-height nil
  "Use varying text heights for org headings."
  :type 'boolean
  :group 'pure-theme)

(defcustom pure-theme-org-highlight nil
  "Highlight org headings."
  :type 'boolean
  :group 'pure-theme)

(defcustom pure-theme-custom-colors nil
  "Specify a list of custom colors"
  :type 'alist
  :group 'pure-theme)

(defun true-color-p ()
  (or
   (display-graphic-p)
   (= (tty-display-color-cells) 16777216)))

(defun custom-colors-override ()
  (mapcar (lambda (x) (list (car x) (cdr x)))
          pure-theme-custom-colors))

(defun create-pure-theme (variant theme-name)
  (dyn-let ((class '((class color) (min-colors 89)))
            ;; mode line active colors
            (act1          (if (eq variant 'dark) (material-color 'black   nil) (material-color 'white   nil)))
            (act2          (if (eq variant 'dark) (material-color 'black   nil) (material-color 'white   nil)))
            ;; foreground
            (base          (if (eq variant 'dark) (material-color 'grey   '100) (material-color 'black   nil)))
            (base-dim      (if (eq variant 'dark) (material-color 'grey   '300) (material-color 'grey   '600)))
            ;; background
            (bg1           (if (eq variant 'dark) (material-color 'black   nil) (material-color 'grey   '100)))
            (bg2           (if (eq variant 'dark) (material-color 'grey   '800) (material-color 'grey   '300)))
            (bg3           (if (eq variant 'dark) (material-color 'black   nil) (material-color 'white   nil)))
            (bg4           (if (eq variant 'dark) (material-color 'black   nil) (material-color 'white   nil)))
            ;; cursor
            (cursor        (if (eq variant 'dark) (material-color 'red    '500) (material-color 'red    '500)))
            ;; line numbers
            (lnum          (if (eq variant 'dark) (material-color 'grey   '600) (material-color 'grey   '600)))
            ;; complementary color/primary/accent
            (comp          (if (eq variant 'dark) (material-color 'blue   '500) (material-color 'blue   '500)))
            (primary       (if (eq variant 'dark) (material-color 'blue   '500) (material-color 'blue   '500)))
            (accent        (if (eq variant 'dark) (material-color 'pink   '400) (material-color 'pink   '400)))
            ;; border
            (border        (if (eq variant 'dark) (material-color 'grey   '800) (material-color 'grey   '200)))
            ;; org-mode code block
            (cblk          (if (eq variant 'dark) (material-color 'grey   '100) (material-color 'grey   '900)))
            (cblk-bg       (if (eq variant 'dark) (material-color 'grey   '800) (material-color 'grey   '200)))
            (cblk-ln       (if (eq variant 'dark) (material-color 'grey   '300) (material-color 'grey   '700)))
            (cblk-ln-bg    (if (eq variant 'dark) (material-color 'grey   '900) (material-color 'grey   '100)))
            ;; programming
            (const         (if (eq variant 'dark) (material-color 'grey   '400) (material-color 'grey   '600)))
            (func          (if (eq variant 'dark) (material-color 'white   nil) (material-color 'black   nil)))
            (keyword       (if (eq variant 'dark) (material-color 'orange  '50) (material-color 'brown '900)))
            (str           (if (eq variant 'dark) (material-color 'bluegrey '300) (material-color 'indigo '900)))
            (type          (if (eq variant 'dark) (material-color 'yellow  '50) (material-color 'blue '900)))
            (var           (if (eq variant 'dark) (material-color 'pink   '100) (material-color 'teal   '900)))
            ;; comments/documentation
            (doc           (if (eq variant 'dark) (material-color 'yellow '300) (material-color 'deeporange '900)))
            (comment       (if (eq variant 'dark) (material-color 'grey   '600) (material-color 'grey   '400)))
            (comment-bg    (if (eq variant 'dark) (material-color 'black   nil) (material-color 'white    nil)))
            ;; success/error/warning
            (suc           (if (eq variant 'dark) (material-color 'green  '500) (material-color 'green  '500)))
            (war           (if (eq variant 'dark) (material-color 'orange '500) (material-color 'orange '500)))
            (err           (if (eq variant 'dark) (material-color 'red    '500) (material-color 'red    '500)))
            ;; headings/org-mode
            (head1         (if (eq variant 'dark) (material-color 'white   nil) (material-color 'black   nil)))
            (head1-bg      (if (eq variant 'dark) (material-color 'grey   '100) (material-color 'grey   '900)))
            (head2         (if (eq variant 'dark) (material-color 'white   nil) (material-color 'black   nil)))
            (head2-bg      (if (eq variant 'dark) (material-color 'grey   '100) (material-color 'grey   '900)))
            (head3         (if (eq variant 'dark) (material-color 'white   nil) (material-color 'black   nil)))
            (head3-bg      (if (eq variant 'dark) (material-color 'grey   '100) (material-color 'grey   '900)))
            (head4         (if (eq variant 'dark) (material-color 'white   nil) (material-color 'black   nil)))
            (head4-bg      (if (eq variant 'dark) (material-color 'grey   '100) (material-color 'grey   '900)))
            ;; highlighting
            ; highlight what?
            (highlight-bg  (if (eq variant 'dark) (material-color 'grey   '800) (material-color 'grey   '300)))
            (highlight     (if (eq variant 'dark) (material-color 'yellow '500) (material-color 'orange '800)))
            (highlight-s   (if (eq variant 'dark) (material-color 'red    '700) (material-color 'red    '300)))
            ; normal search (/ or *)
            (search-bg     (if (eq variant 'dark) (material-color 'blue '900) (material-color 'blue '100)))
            (search-fg     (if (eq variant 'dark) (material-color 'yellow '500) (material-color 'orange '800)))
            (search-fg-s   (if (eq variant 'dark) (material-color 'yellow '500) (material-color 'orange   '800)))
            ; highlight current variable
            (mat           (if (eq variant 'dark) (material-color 'green  '200) (material-color 'green '800)))
            (mat-s         (if (eq variant 'dark) (material-color 'red    '500) (material-color 'red    '500)))
            ;; misc TODO
            (meta          (if (eq variant 'dark) (material-color 'grey   '400) (material-color 'grey   '600)))
            (ttip          (if (eq variant 'dark) (material-color 'grey   '400) (material-color 'grey   '600)))
            (ttip-sl       (if (eq variant 'dark) (material-color 'grey   '500) (material-color 'grey   '500)))
            (ttip-bg       (if (eq variant 'dark) (material-color 'grey   '600) (material-color 'grey   '400)))

            ;; basic colors
            (aqua          (if (eq variant 'dark) (material-color 'cyan   '500) (material-color 'cyan   '500)))
            (aqua-bg       (if (eq variant 'dark) (material-color 'cyan   '900) (material-color 'cyan   '100)))
            (green         (if (eq variant 'dark) (material-color 'green  '500) (material-color 'green  '500)))
            (green-bg      (if (eq variant 'dark) (material-color 'green  '900) (material-color 'green  '100)))
            (green-bg-s    (if (eq variant 'dark) (material-color 'green  '900) (material-color 'green  '100)))
            (cyan          (if (eq variant 'dark) (material-color 'cyan   '500) (material-color 'cyan   '500)))
            (red           (if (eq variant 'dark) (material-color 'red    '500) (material-color 'red    '500)))
            (red-bg        (if (eq variant 'dark) (material-color 'red    '900) (material-color 'red    '100)))
            (red-bg-s      (if (eq variant 'dark) (material-color 'red    '900) (material-color 'red    '100)))
            (blue          (if (eq variant 'dark) (material-color 'blue   '500) (material-color 'blue   '500)))
            (blue-bg       (if (eq variant 'dark) (material-color 'blue   '900) (material-color 'blue   '100)))
            (magenta       (if (eq variant 'dark) (material-color 'purple '500) (material-color 'purple '500)))
            (yellow        (if (eq variant 'dark) (material-color 'yellow '500) (material-color 'yellow '900)))
            (yellow-bg     (if (eq variant 'dark) (material-color 'yellow '900) (material-color 'yellow '100))))

           custom-colors-override

           (custom-theme-set-faces
            theme-name

;;;;; basics
            `(cursor ((,class (:background ,cursor))))
            `(custom-button ((,class :background ,bg2 :foreground ,base :box (:line-width 2 :style released-button))))
            `(default ((,class (:background ,bg1 :foreground ,base))))
            `(default-italic ((,class (:italic t))))
            `(error ((,class (:foreground ,err))))
            `(eval-sexp-fu-flash ((,class (:background ,suc :foreground ,bg1))))
            `(eval-sexp-fu-flash-error ((,class (:background ,err :foreground ,bg1))))
            `(font-lock-builtin-face ((,class (:foreground ,keyword))))
            `(font-lock-comment-face ((,class (:foreground ,comment :background ,(when pure-theme-comment-bg comment-bg)))))
            `(highlight-doxygen-comment ((,class (:foreground ,doc :background ,(when pure-theme-comment-bg comment-bg)))))
            `(font-lock-constant-face ((,class (:foreground ,const))))
            `(font-lock-doc-face ((,class (:foreground ,doc))))
            `(font-lock-function-name-face ((,class (:foreground ,func :inherit bold))))
            `(font-lock-keyword-face ((,class (:inherit bold :foreground ,keyword))))
            `(font-lock-negation-char-face ((,class (:foreground ,const))))
            `(font-lock-preprocessor-face ((,class (:foreground ,base-dim))))
            `(font-lock-reference-face ((,class (:foreground ,const))))
            `(font-lock-string-face ((,class (:foreground ,str))))
            `(highlight-numbers-number ((,class (:foreground ,str))))
            `(font-lock-type-face ((,class (:foreground ,type))))
            `(font-lock-variable-name-face ((,class (:foreground ,var))))
            `(font-lock-warning-face ((,class (:foreground ,war :background ,bg1))))
            `(fringe ((,class (:background ,bg1 :foreground ,base))))
            `(header-line ((,class :background ,bg4)))
            `(highlight ((,class (:foreground ,mat :inherit bold))))
            `(hl-line ((,class (:background ,bg2))))
            `(isearch ((,class (:foreground ,search-fg-s :inherit bold))))
            `(italic ((,class (:italic t))))
            `(lazy-highlight ((,class (:foreground ,search-fg :weight bold))))
            `(link ((,class (:foreground ,comment :underline t))))
            `(link-visited ((,class (:foreground ,comp :underline t))))
            `(match ((,class (:background ,highlight-s :foreground ,mat-s :inherit bold))))
            `(minibuffer-prompt ((,class (:inherit bold :foreground ,keyword))))
            `(page-break-lines ((,class (:foreground ,act2))))
            `(region ((,class (:background ,highlight-bg))))
            `(secondary-selection ((,class (:background ,bg3))))
            `(success ((,class (:foreground ,suc))))
            `(tooltip ((,class (:background ,ttip-sl :foreground ,base :bold nil :italic nil :underline nil))))
            `(vertical-border ((,class (:foreground ,bg4))))
            `(warning ((,class (:foreground ,war))))

;;;;; ahs
            `(ahs-face ((,class (:background ,highlight-bg))))
            `(ahs-plugin-whole-buffer-face ((,class (:background ,mat :foreground ,bg1))))

;;;;; anzu-mode
            `(anzu-mode-line ((,class (:foreground ,yellow :inherit bold))))

;;;;; auto-complete
            `(ac-completion-face ((,class (:background ,ttip-bg :foreground ,ttip))))

;;;;; avy
            `(avy-lead-face   ((,class (:background ,blue-bg :foreground ,magenta))))
            `(avy-lead-face-0 ((,class (:background ,blue-bg :foreground ,blue))))
            `(avy-lead-face-1 ((,class (:background ,blue-bg :foreground ,magenta))))
            `(avy-lead-face-2 ((,class (:background ,blue-bg :foreground ,blue))))

;;;;; cider
            `(cider-enlightened ((,class (:background nil :box (:color ,yellow :line-width -1 :style nil) :foreground ,yellow))))
            `(cider-enlightened-local ((,class (:foreground ,yellow))))
            `(cider-instrumented-face ((,class (:background nil :box (:color ,red :line-width -1 :style nil) :foreground ,red))))
            `(cider-result-overlay-face ((,class (:background nil :box (:color ,blue :line-width -1 :style nil) :foreground ,blue))))
            `(cider-test-error-face ((,class (:background ,war :foreground ,bg1))))
            `(cider-test-failure-face ((,class (:background ,err :foreground ,bg1))))
            `(cider-test-success-face ((,class (:background ,suc :foreground ,bg1))))
            `(cider-traced-face ((,class :box (:color ,cyan :line-width -1 :style nil))))

;;;;; company
            `(company-echo-common ((,class (:background ,base :foreground ,bg1))))
            `(company-preview ((,class (:background ,ttip-bg :foreground ,ttip))))
            `(company-preview-common ((,class (:background ,ttip-bg :foreground ,base))))
            `(company-preview-search ((,class (:inherit match))))
            `(company-scrollbar-bg ((,class (:background ,bg2))))
            `(company-scrollbar-fg ((,class (:background ,act2))))
            `(company-template-field ((,class (:inherit region))))
            `(company-tooltip ((,class (:background ,ttip-bg :foreground ,ttip))))
            `(company-tooltip-annotation ((,class (:foreground ,keyword))))
            `(company-tooltip-common ((,class (:background ,ttip-bg :foreground ,base))))
            `(company-tooltip-common-selection ((,class (:foreground ,base))))
            `(company-tooltip-mouse ((,class (:inherit highlight))))
            `(company-tooltip-search ((,class (:inherit match))))
            `(company-tooltip-selection ((,class (:background ,ttip-sl :foreground ,base))))

;;;;; diff
            `(diff-added             ((,class :background nil :foreground ,green)))
            `(diff-changed           ((,class :background nil :foreground ,keyword)))
            `(diff-header            ((,class :background ,cblk-ln-bg :foreground ,func)))
            `(diff-indicator-added   ((,class :background nil :foreground ,green)))
            `(diff-indicator-changed ((,class :background nil :foreground ,keyword)))
            `(diff-indicator-removed ((,class :background nil :foreground ,red)))
            `(diff-refine-added      ((,class :background ,green :foreground ,bg4)))
            `(diff-refine-changed    ((,class :background ,keyword :foreground ,bg4)))
            `(diff-refine-removed    ((,class :background ,red :foreground ,bg4)))
            `(diff-removed           ((,class :background nil :foreground ,red)))

;;;;; diff-hl
            `(diff-hl-change ((,class :background ,blue-bg :foreground ,blue)))
            `(diff-hl-delete ((,class :background ,red-bg :foreground ,red)))
            `(diff-hl-insert ((,class :background ,green-bg :foreground ,green)))

;;;;; dired
            `(dired-directory ((,class (:foreground ,keyword :background ,bg1 :inherit bold))))
            `(dired-flagged ((,class (:foreground ,red))))
            `(dired-header ((,class (:foreground ,comp :inherit bold))))
            `(dired-ignored ((,class (:inherit shadow))))
            `(dired-mark ((,class (:foreground ,comp :inherit bold))))
            `(dired-marked ((,class (:foreground ,magenta :inherit bold))))
            `(dired-perm-write ((,class (:foreground ,base :underline t))))
            `(dired-symlink ((,class (:foreground ,cyan :background ,bg1 :inherit bold))))
            `(dired-warning ((,class (:foreground ,war))))

;;;;; ediff
            `(ediff-current-diff-A ((,class(:background ,red-bg-s :foreground ,red))))
            `(ediff-current-diff-Ancestor ((,class(:background ,aqua-bg :foreground ,aqua))))
            `(ediff-current-diff-B ((,class(:background ,green-bg-s :foreground ,green))))
            `(ediff-current-diff-C ((,class(:background ,blue-bg :foreground ,blue))))
            `(ediff-even-diff-A ((,class(:background ,bg3))))
            `(ediff-even-diff-Ancestor ((,class(:background ,bg3))))
            `(ediff-even-diff-B ((,class(:background ,bg3))))
            `(ediff-even-diff-C ((,class(:background ,bg3))))
            `(ediff-fine-diff-A ((,class(:background nil :inherit bold))))
            `(ediff-fine-diff-Ancestor ((,class(:background nil :inherit bold))))
            `(ediff-fine-diff-B ((,class(:background nil :inherit bold))))
            `(ediff-fine-diff-C ((,class(:background nil :inherit bold))))
            `(ediff-odd-diff-A ((,class(:background ,bg4))))
            `(ediff-odd-diff-Ancestor ((,class(:background ,bg4))))
            `(ediff-odd-diff-B ((,class(:background ,bg4))))
            `(ediff-odd-diff-C ((,class(:background ,bg4))))

;;;;; ein
            `(ein:cell-input-area((,class (:background ,bg2))))
            `(ein:cell-input-prompt ((,class (:foreground ,suc))))
            `(ein:cell-output-prompt ((,class (:foreground ,err))))
            `(ein:notification-tab-normal ((,class (:foreground ,keyword))))
            `(ein:notification-tab-selected ((,class (:foreground ,suc :inherit bold))))

;;;;; eldoc
            `(eldoc-highlight-function-argument ((,class (:foreground ,mat :inherit bold))))

;;;;; enh-ruby
            `(enh-ruby-string-delimiter-face ((,class (:foreground ,str))))
            `(enh-ruby-op-face ((,class (:background ,bg1 :foreground ,base))))

;;;;; erc
            `(erc-input-face ((,class (:foreground ,func))))
            `(erc-my-nick-face ((,class (:foreground ,keyword))))
            `(erc-nick-default-face ((,class (:foreground ,keyword))))
            `(erc-nick-prefix-face ((,class (:foreground ,yellow))))
            `(erc-notice-face ((,class (:foreground ,str))))
            `(erc-prompt-face ((,class (:foreground ,mat :inherit bold))))
            `(erc-timestamp-face ((,class (:foreground ,keyword))))

;;;;; eshell
            `(eshell-ls-archive ((,class (:foreground ,red :inherit bold))))
            `(eshell-ls-backup ((,class (:inherit font-lock-comment-face))))
            `(eshell-ls-clutter ((,class (:inherit font-lock-comment-face))))
            `(eshell-ls-directory ((,class (:foreground ,keyword :inherit bold))))
            `(eshell-ls-executable ((,class (:foreground ,suc :inherit bold))))
            `(eshell-ls-missing ((,class (:inherit font-lock-warning-face))))
            `(eshell-ls-product ((,class (:inherit font-lock-doc-face))))
            `(eshell-ls-special ((,class (:foreground ,yellow :inherit bold))))
            `(eshell-ls-symlink ((,class (:foreground ,cyan :inherit bold))))
            `(eshell-ls-unreadable ((,class (:foreground ,base))))
            `(eshell-prompt ((,class (:foreground ,keyword :inherit bold))))

;;;;; evil
            `(evil-ex-substitute-matches ((,class (:background ,red-bg :foreground ,red))))
            `(evil-ex-substitute-replacement ((,class (:background ,green-bg :foreground ,green))))

;;;;; flycheck
            `(flycheck-error
              ((,(append '((supports :underline (:style line))) class)
                (:underline (:style line :color ,err)))
               (,class (:foreground ,base :background ,err :inherit bold :underline t))))
            `(flycheck-error-list-checker-name ((,class (:foreground ,keyword))))
            `(flycheck-fringe-error ((,class (:foreground ,err :inherit bold))))
            `(flycheck-fringe-info ((,class (:foreground ,keyword :inherit bold))))
            `(flycheck-fringe-warning ((,class (:foreground ,war :inherit bold))))
            `(flycheck-info
              ((,(append '((supports :underline (:style line))) class)
                (:underline (:style line :color ,keyword)))
               (,class (:foreground ,base :background ,keyword :inherit bold :underline t))))
            `(flycheck-warning
              ((,(append '((supports :underline (:style line))) class)
                (:underline (:style line :color ,war)))
               (,class (:foreground ,base :background ,war :inherit bold :underline t))))

;;;;; git-gutter-fr
            `(git-gutter-fr:added ((,class (:foreground ,green :inherit bold))))
            `(git-gutter-fr:deleted ((,class (:foreground ,red :inherit bold))))
            `(git-gutter-fr:modified ((,class (:foreground ,blue :inherit bold))))

;;;;; git-timemachine
            `(git-timemachine-minibuffer-detail-face ((,class (:foreground ,blue :inherit bold :background ,blue-bg))))

;;;;; gnus
            `(gnus-emphasis-highlight-words ((,class (:background ,suc :foreground ,bg1))))
            `(gnus-header-content ((,class (:foreground ,keyword))))
            `(gnus-header-from ((,class (:foreground ,var))))
            `(gnus-header-name ((,class (:foreground ,comp))))
            `(gnus-header-subject ((,class (:foreground ,func :inherit bold))))
            `(gnus-summary-cancelled ((,class (:background ,war :foreground ,bg1))))

;;;;; guide-key
            `(guide-key/highlight-command-face ((,class (:foreground ,base))))
            `(guide-key/key-face ((,class (:foreground ,keyword))))
            `(guide-key/prefix-command-face ((,class (:foreground ,keyword :inherit bold))))

;;;;; helm
            `(helm-bookmark-directory ((,class (:inherit helm-ff-directory))))
            `(helm-bookmark-file ((,class (:foreground ,base))))
            `(helm-bookmark-gnus ((,class (:foreground ,comp))))
            `(helm-bookmark-info ((,class (:foreground ,comp))))
            `(helm-bookmark-man ((,class (:foreground ,comp))))
            `(helm-bookmark-w3m ((,class (:foreground ,comp))))
            `(helm-buffer-directory ((,class (:foreground ,base :background ,bg1))))
            `(helm-buffer-file ((,class (:foreground ,base :background ,bg1))))
            `(helm-buffer-not-saved ((,class (:foreground ,comp :background ,bg1))))
            `(helm-buffer-process ((,class (:foreground ,keyword :background ,bg1))))
            `(helm-buffer-saved-out ((,class (:foreground ,base :background ,bg1))))
            `(helm-buffer-size ((,class (:foreground ,base :background ,bg1))))
            `(helm-candidate-number ((,class (:background ,bg1 :foreground ,keyword :inherit bold))))
            `(helm-ff-directory ((,class (:foreground ,keyword :background ,bg1 :inherit bold))))
            `(helm-ff-dotted-directory ((,class (:foreground ,keyword :background ,bg1 :inherit bold))))
            `(helm-ff-dotted-symlink-directory ((,class (:foreground ,cyan :background ,bg1 :inherit bold))))
            `(helm-ff-executable ((,class (:foreground ,suc :background ,bg1 :weight normal))))
            `(helm-ff-file ((,class (:foreground ,base :background ,bg1 :weight normal))))
            `(helm-ff-invalid-symlink ((,class (:foreground ,red :background ,bg1 :inherit bold))))
            `(helm-ff-prefix ((,class (:foreground ,bg1 :background ,keyword :weight normal))))
            `(helm-ff-symlink ((,class (:foreground ,cyan :background ,bg1 :inherit bold))))
            `(helm-grep-cmd-line ((,class (:foreground ,base :background ,bg1))))
            `(helm-grep-file ((,class (:foreground ,base :background ,bg1))))
            `(helm-grep-finish ((,class (:foreground ,base :background ,bg1))))
            `(helm-grep-lineno ((,class (:foreground ,type :background ,bg1 :inherit bold))))
            `(helm-grep-match ((,class (:foreground nil :background nil :inherit helm-match))))
            `(helm-header ((,class (:foreground ,base :background ,bg1 :underline nil :box nil))))
            `(helm-header-line-left-margin ((,class (:foreground ,keyword :background ,nil))))
            `(helm-match ((,class (:background ,highlight-bg :foreground ,base))))
            `(helm-match-item ((,class (:background ,highlight-bg :foreground ,base))))
            `(helm-moccur-buffer ((,class (:foreground ,var :background ,bg1))))
            `(helm-selection ((,class (:foreground ,highlight :background ,highlight-bg :inherit bold))))
            `(helm-selection-line ((,class (:background ,bg2))))
            `(helm-separator ((,class (:foreground ,base :background ,primary :inherit bold))))
            `(helm-source-header ((,class (:background ,primary :foreground ,base :inherit bold))))
            `(helm-time-zone-current ((,class (:foreground ,keyword :background ,bg1))))
            `(helm-time-zone-home ((,class (:foreground ,comp :background ,bg1))))
            `(helm-visible-mark ((,class (:foreground ,keyword :background ,bg3))))

;;;;; helm-swoop
            `(helm-swoop-target-line-block-face ((,class (:foreground ,base :background ,highlight))))
            `(helm-swoop-target-line-face ((,class (:background ,highlight))))
            `(helm-swoop-target-word-face ((,class (:background ,highlight :foreground ,mat))))

;;;;; highlights
            `(hi-yellow ((,class (:foreground ,yellow :background ,yellow-bg))))
            `(hi-green  ((,class (:foreground ,green :background ,green-bg))))

;;;;; highlight-indentation
            `(highlight-indentation-face ((,class (:background ,comment-bg))))

;;;;; highlight-symbol
            `(highlight-symbol-face ((,class (:background ,bg2))))

;;;;; hydra
            `(hydra-face-blue ((,class (:foreground ,blue))))
            `(hydra-face-red ((,class (:foreground ,red))))

;;;;; ido
            `(ido-first-match ((,class (:foreground ,comp :inherit bold))))
            `(ido-only-match ((,class (:foreground ,mat :inherit bold))))
            `(ido-subdir ((,class (:foreground ,keyword))))
            `(ido-vertical-match-face ((,class (:foreground ,comp :underline nil))))

;;;;; info
            `(info-header-xref ((,class (:foreground ,func :underline t))))
            `(info-menu ((,class (:foreground ,suc))))
            `(info-node ((,class (:foreground ,func :inherit bold))))
            `(info-quoted-name ((,class (:foreground ,keyword))))
            `(info-reference-item ((,class (:background nil :underline t :inherit bold))))
            `(info-string ((,class (:foreground ,str))))
            `(info-title-1 ((,class (:height 1.4 :inherit bold))))
            `(info-title-2 ((,class (:height 1.3 :inherit bold))))
            `(info-title-3 ((,class (:height 1.3))))
            `(info-title-4 ((,class (:height 1.2))))

;;;;; ivy
            `(ivy-current-match ((,class (:background ,highlight-bg :inherit bold))))
            `(ivy-minibuffer-match-face-1 ((,class (:background ,highlight-bg))))
            `(ivy-minibuffer-match-face-2 ((,class (:foreground ,highlight))))
            `(ivy-minibuffer-match-face-3 ((,class (:foreground ,highlight))))
            `(ivy-minibuffer-match-face-4 ((,class (:foreground ,highlight))))
            `(ivy-remote ((,class (:foreground ,cyan))))

;;;;; latex
            `(font-latex-bold-face ((,class (:foreground ,comp))))
            `(font-latex-italic-face ((,class (:foreground ,keyword :italic t))))
            `(font-latex-match-reference-keywords ((,class (:foreground ,const))))
            `(font-latex-match-variable-keywords ((,class (:foreground ,var))))
            `(font-latex-sectioning-0-face ((,class (:inherit bold :foreground ,head3 :height ,(if pure-theme-org-height 1.3 1.0) :background ,(when pure-theme-org-highlight head3-bg)))))
            `(font-latex-sectioning-1-face ((,class (:inherit bold :foreground ,head4 :height ,(if pure-theme-org-height 1.3 1.0) :background ,(when pure-theme-org-highlight head4-bg)))))
            `(font-latex-sectioning-2-face ((,class (:inherit bold :foreground ,head1 :height ,(if pure-theme-org-height 1.3 1.0) :background ,(when pure-theme-org-highlight head1-bg)))))
            `(font-latex-sectioning-3-face ((,class (:inherit bold :foreground ,head2 :height ,(if pure-theme-org-height 1.2 1.0) :background ,(when pure-theme-org-highlight head2-bg)))))
            `(font-latex-sectioning-4-face ((,class (:bold nil :foreground ,head3 :height ,(if pure-theme-org-height 1.1 1.0) :background ,(when pure-theme-org-highlight head3-bg)))))
            `(font-latex-sectioning-5-face ((,class (:bold nil :foreground ,head4 :background ,(when pure-theme-org-highlight head4-bg)))))
            `(font-latex-string-face ((,class (:foreground ,str))))

;;;;; linum-mode
            `(linum ((,class (:foreground ,lnum :background ,bg1))))

;;;;; linum-relative
            `(linum-relative-current-face ((,class (:foreground ,comp))))

;;;;; magit
            `(magit-blame-culprit ((,class :background ,bg2 :foreground ,base)))
            `(magit-blame-header  ((,class :background ,bg2 :foreground ,base)))
            `(magit-blame-sha1    ((,class :background ,bg2 :foreground ,base)))
            `(magit-blame-subject ((,class :background ,bg2 :foreground ,base)))
            `(magit-blame-time    ((,class :background ,bg2 :foreground ,base)))
            `(magit-blame-name    ((,class :background ,bg2 :foreground ,base)))
            `(magit-blame-heading ((,class :background ,bg2 :foreground ,base)))
            `(magit-blame-hash    ((,class :background ,bg2 :foreground ,base)))
            `(magit-blame-summary ((,class :background ,bg2 :foreground ,base)))
            `(magit-blame-date    ((,class :background ,bg2 :foreground ,base)))
            `(magit-branch ((,class (:foreground ,const :inherit bold))))
            `(magit-branch-current ((,class (:background ,blue-bg :foreground ,blue :inherit bold :box t))))
            `(magit-branch-local ((,class (:background ,blue-bg :foreground ,blue :inherit bold))))
            `(magit-branch-remote ((,class (:background ,aqua-bg :foreground ,aqua :inherit bold))))
            `(magit-diff-context-highlight ((,class (:background ,bg2 :foreground ,base))))
            `(magit-diff-file-header ((,class (:background ,comment-bg :foreground ,comment))))
            `(magit-diff-file-heading ((,class (:background ,comment-bg :foreground ,comment))))
            `(magit-diff-file-heading-highlight ((,class (:background ,comment-bg :foreground ,comment))))
            `(magit-diff-hunk-header ((,class (:background ,ttip-bg :foreground ,ttip))))
            `(magit-diff-hunk-heading ((,class (:background ,ttip-bg :foreground ,ttip))))
            `(magit-diff-hunk-heading-highlight ((,class (:background ,ttip-bg :foreground ,ttip))))
            `(magit-hash ((,class (:foreground ,var))))
            `(magit-hunk-heading           ((,class (:background ,bg3))))
            `(magit-hunk-heading-highlight ((,class (:background ,bg3))))
            `(magit-item-highlight ((,class :background ,bg2)))
            `(magit-log-author ((,class (:foreground ,func))))
            `(magit-log-head-label-head ((,class (:background ,yellow :foreground ,bg1 :inherit bold))))
            `(magit-log-head-label-local ((,class (:background ,keyword :foreground ,bg1 :inherit bold))))
            `(magit-log-head-label-remote ((,class (:background ,suc :foreground ,bg1 :inherit bold))))
            `(magit-log-head-label-tags ((,class (:background ,magenta :foreground ,bg1 :inherit bold))))
            `(magit-log-head-label-wip ((,class (:background ,cyan :foreground ,bg1 :inherit bold))))
            `(magit-log-sha1 ((,class (:foreground ,str))))
            `(magit-process-ng ((,class (:foreground ,war :inherit bold))))
            `(magit-process-ok ((,class (:foreground ,func :inherit bold))))
            `(magit-section-heading        ((,class (:foreground ,keyword :inherit bold))))
            `(magit-section-highlight      ((,class (:background ,bg2))))
            `(magit-section-title ((,class (:background ,bg1 :foreground ,keyword :inherit bold))))

;;;;; man
            `(Man-overstrike ((,class (:foreground ,head1 :inherit bold))))
            `(Man-reverse ((,class (:foreground ,highlight))))
            `(Man-underline ((,class (:foreground ,comp :underline t))))

;;;;; markdown
            `(markdown-header-face-1 ((,class (:inherit bold :foreground ,head1 :height ,(if pure-theme-org-height 1.3 1.0) :background ,(when pure-theme-org-highlight head1-bg)))))
            `(markdown-header-face-2 ((,class (:inherit bold :foreground ,head2 :height ,(if pure-theme-org-height 1.2 1.0) :background ,(when pure-theme-org-highlight head2-bg)))))
            `(markdown-header-face-3 ((,class (:bold nil :foreground ,head3 :height ,(if pure-theme-org-height 1.1 1.0) :background ,(when pure-theme-org-highlight head3-bg)))))
            `(markdown-header-face-4 ((,class (:bold nil :foreground ,head4 :background ,(when pure-theme-org-highlight head4-bg)))))
            `(markdown-header-face-5 ((,class (:bold nil :foreground ,head1))))
            `(markdown-header-face-6 ((,class (:bold nil :foreground ,head2))))

;;;;; mode-line
            `(mode-line           ((,class (:foreground ,base :background ,act1 :box (:color ,border :line-width 1)))))
            `(mode-line-inactive  ((,class (:foreground ,base :background ,bg1  :box (:color ,border :line-width 1)))))
            `(mode-line-buffer-id ((,class (:inherit bold :foreground ,func))))

;;;;; mu4e
            `(mu4e-cited-1-face ((,class (:foreground ,base))))
            `(mu4e-cited-7-face ((,class (:foreground ,base))))
            `(mu4e-header-marks-face ((,class (:foreground ,comp))))
            `(mu4e-header-key-face ((,class (:foreground ,head2 :inherit bold))))
            `(mu4e-view-url-number-face ((,class (:foreground ,comp))))
            `(mu4e-unread-face ((,class (:foreground ,yellow :inherit bold))))

;;;;; neotree
            `(neo-dir-link-face ((,class (:foreground ,keyword :inherit bold))))
            `(neo-expand-btn-face ((,class (:foreground ,base))))
            `(neo-file-link-face ((,class (:foreground ,base))))
            `(neo-root-dir-face ((,class (:foreground ,func :inherit bold))))

;;;;; org
            `(org-agenda-clocking ((,class (:background ,bg2 :foreground ,yellow))))
            `(org-agenda-date ((,class (:foreground ,var :height ,(if pure-theme-org-height 1.1 1.0)))))
            `(org-agenda-date-today ((,class (:foreground ,keyword :slant italic :inherit bold :height ,(if pure-theme-org-height 1.3 1.0)))))
            `(org-agenda-date-weekend ((,class (:inherit bold :foreground ,var))))
            `(org-agenda-done ((,class (:foreground ,suc :height ,(if pure-theme-org-height 1.2 1.0)))))
            `(org-agenda-structure ((,class (:inherit bold :foreground ,comp))))
            `(org-block ((,class (:background ,cblk-bg :foreground ,cblk))))
            `(org-block-begin-line ((,class (:background ,cblk-ln-bg :foreground ,cblk-ln))))
            `(org-block-end-line ((,class (:background ,cblk-ln-bg :foreground ,cblk-ln))))
            `(org-clock-overlay ((,class (:foreground ,comp))))
            `(org-code ((,class (:background ,cblk-bg :foreground ,cblk))))
            `(org-column ((,class (:background ,highlight-bg))))
            `(org-column-title ((,class (:background ,highlight-bg))))
            `(org-date ((,class (:underline t :foreground ,var))))
            `(org-date-selected ((,class (:background ,func :foreground ,bg1))))
            `(org-document-info-keyword ((,class (:foreground ,meta))))
            `(org-document-title ((,class (:foreground ,func :inherit bold :height ,(if pure-theme-org-height 1.4 1.0) :underline t))))
            `(org-done ((,class (:foreground ,suc :inherit bold))))
            `(org-ellipsis ((,class (:foreground ,base-dim))))
            `(org-footnote  ((,class (:underline t :foreground ,base))))
            `(org-headline-done ((,class (:foreground ,base-dim))))
            `(org-hide ((,class (:foreground ,bg1 :background ,bg1))))
            `(org-indent ((,class (:foreground ,bg1 :background ,bg1))))
            `(org-kbd ((,class (:inherit region :foreground ,base :box (:line-width 1 :style released-button)))))
            `(org-level-1 ((,class (:inherit bold :foreground ,head1 :background ,(when pure-theme-org-highlight head1-bg) :height ,(if pure-theme-org-height 1.3 1.0)))))
            `(org-level-2 ((,class (:inherit bold :foreground ,head2 :background ,(when pure-theme-org-highlight head2-bg) :height ,(if pure-theme-org-height 1.2 1.0)))))
            `(org-level-3 ((,class (:inherit bold :foreground ,head3 :background ,(when pure-theme-org-highlight head3-bg) :height ,(if pure-theme-org-height 1.1 1.0)))))
            `(org-level-4 ((,class (:inherit bold :foreground ,head4 :background ,(when pure-theme-org-highlight head4-bg)))))
            `(org-level-5 ((,class (:bold nil :foreground ,head1))))
            `(org-level-6 ((,class (:bold nil :foreground ,head2))))
            `(org-level-7 ((,class (:bold nil :foreground ,head3))))
            `(org-level-8 ((,class (:bold nil :foreground ,head4))))
            `(org-link ((,class (:underline t :foreground ,comment))))
            `(org-meta-line ((,class (:foreground ,meta))))
            `(org-mode-line-clock-overrun ((,class (:foreground ,err))))
            `(org-priority ((,class (:foreground ,war :inherit bold))))
            `(org-quote ((,class (:inherit org-block :slant italic))))
            `(org-scheduled ((,class (:foreground ,base-dim))))
            `(org-scheduled-today ((,class (:foreground ,func :height ,(if pure-theme-org-height 1.2 1.0)))))
            `(org-sexp-date ((,class (:foreground ,base))))
            `(org-special-keyword ((,class (:foreground ,func))))
            `(org-table ((,class (:foreground ,base))))
            `(org-tag ((,class (:bold nil :foreground ,base-dim))))
            `(org-time-grid ((,class (:foreground ,str))))
            `(org-todo ((,class (:foreground ,err :inherit bold))))
            `(org-verbatim ((,class (:foreground ,keyword))))
            `(org-verse ((,class (:inherit org-block :slant italic))))
            `(org-warning ((,class (:foreground ,err))))

;;;;; perspective
            `(persp-selected-face ((,class (:inherit bold :foreground ,func))))

;;;;; popup
            `(popup-face ((,class (:background ,ttip-bg :foreground ,ttip))))
            `(popup-tip-face ((,class (:background ,ttip-sl :foreground ,base :bold nil :italic nil :underline nil))))
            `(popup-menu-face ((,class (:background ,ttip-bg :foreground ,base))))
            `(popup-enu-selection-face ((,class (:background ,ttip-sl :foreground ,base))))
            `(popup-menu-mouse-face ((,class (:inherit highlight))))
            `(popup-isearch-match ((,class (:inherit match))))
            `(popup-scroll-bar-foreground-face ((,class (:background ,act2))))
            `(popup-scroll-bar-background-face ((,class (:background ,bg2))))

;;;;; powerline
            `(powerline-active1 ((,class (:background ,act1 :foreground ,base))))
            `(powerline-active2 ((,class (:background ,act2 :foreground ,base))))
            `(powerline-inactive1 ((,class (:background ,bg2 :foreground ,base))))
            `(powerline-inactive2 ((,class (:background ,bg2 :foreground ,base))))

;;;;; rainbow-delimiters
            `(rainbow-delimiters-depth-1-face ((,class :foreground ,keyword)))
            `(rainbow-delimiters-depth-2-face ((,class :foreground ,func)))
            `(rainbow-delimiters-depth-3-face ((,class :foreground ,str)))
            `(rainbow-delimiters-depth-4-face ((,class :foreground ,green)))
            `(rainbow-delimiters-depth-5-face ((,class :foreground ,yellow)))
            `(rainbow-delimiters-depth-6-face ((,class :foreground ,keyword)))
            `(rainbow-delimiters-depth-7-face ((,class :foreground ,func)))
            `(rainbow-delimiters-depth-8-face ((,class :foreground ,str)))
            `(rainbow-delimiters-unmatched-face ((,class :foreground ,err :overline t)))
            `(rainbow-delimiters-mismatched-face ((,class :foreground ,err :overline t)))

;;;;; shm
            `(shm-current-face ((,class (:background ,bg2))))
            `(shm-quarantine-face ((,class (:background ,red-bg-s))))

;;;;; show-paren
            `(show-paren-match ((,class (:background ,green-bg-s))))
            `(show-paren-mismatch ((,class (:background ,red-bg-s))))

;;;;; smartparens
            `(sp-pair-overlay-face ((,class (:background ,highlight-bg :foreground nil))))
            `(sp-show-pair-match-face ((,class (:foreground ,mat :inherit bold :underline t))))

;;;;; spaceline
            `(spaceline-python-venv ((,class (:foreground ,comp))))
            `(spaceline-flycheck-error  ((,class (:foreground ,err))))
            `(spaceline-flycheck-info   ((,class (:foreground ,keyword))))
            `(spaceline-flycheck-warning((,class (:foreground ,war))))

;;;;; spacemacs-specific
            `(pure-transient-state-title-face ((,class (:background nil :foreground ,comp :box nil :inherit bold))))

;;;;; swiper
            `(swiper-line-face ((,class (:background ,highlight-bg :inherit bold))))
            `(swiper-match-face-1 ((,class (:inherit bold))))
            `(swiper-match-face-2 ((,class (:foreground ,head1 :underline t))))
            `(swiper-match-face-3 ((,class (:foreground ,head4 :underline t))))
            `(swiper-match-face-4 ((,class (:foreground ,head3 :underline t))))

;;;;; term
            `(term ((,class (:foreground ,base :background ,bg1))))
            `(term-color-black ((,class (:foreground ,bg4))))
            `(term-color-blue ((,class (:foreground ,keyword))))
            `(term-color-cyan ((,class (:foreground ,cyan))))
            `(term-color-green ((,class (:foreground ,green))))
            `(term-color-magenta ((,class (:foreground ,magenta))))
            `(term-color-red ((,class (:foreground ,red))))
            `(term-color-white ((,class (:foreground ,base))))
            `(term-color-yellow ((,class (:foreground ,yellow))))

;;;;; web-mode
            `(web-mode-builtin-face ((,class (:inherit ,font-lock-builtin-face))))
            `(web-mode-comment-face ((,class (:inherit ,font-lock-comment-face))))
            `(web-mode-constant-face ((,class (:inherit ,font-lock-constant-face))))
            `(web-mode-doctype-face ((,class (:inherit ,font-lock-comment-face))))
            `(web-mode-function-name-face ((,class (:inherit ,font-lock-function-name-face))))
            `(web-mode-html-attr-name-face ((,class (:foreground ,func))))
            `(web-mode-html-attr-value-face ((,class (:foreground ,keyword))))
            `(web-mode-html-tag-face ((,class (:foreground ,keyword))))
            `(web-mode-keyword-face ((,class (:foreground ,keyword))))
            `(web-mode-string-face ((,class (:foreground ,str))))
            `(web-mode-symbol-face ((,class (:foreground ,type))))
            `(web-mode-type-face ((,class (:inherit ,font-lock-type-face))))
            `(web-mode-warning-face ((,class (:inherit ,font-lock-warning-face))))

;;;;; which-key
            `(which-key-command-description-face ((,class (:foreground ,base))))
            `(which-key-group-description-face ((,class (:foreground ,keyword))))
            `(which-key-key-face ((,class (:foreground ,func :inherit bold))))
            `(which-key-separator-face ((,class (:background nil :foreground ,str))))
            `(which-key-special-key-face ((,class (:background ,func :foreground ,bg1))))

;;;;; which-function-mode
            `(which-func ((,class (:foreground ,func))))

;;;;; whitespace-mode
            `(whitespace-empty ((,class (:background nil :foreground ,yellow))))
            `(whitespace-indentation ((,class (:background nil :foreground ,war))))
            `(whitespace-line ((,class (:background nil :foreground ,comp))))
            `(whitespace-newline ((,class (:background nil :foreground ,comp))))
            `(whitespace-space ((,class (:background nil :foreground ,act2))))
            `(whitespace-space-after-tab ((,class (:background nil :foreground ,yellow))))
            `(whitespace-space-before-tab ((,class (:background nil :foreground ,yellow))))
            `(whitespace-tab ((,class (:background nil))))
            `(whitespace-trailing ((,class (:background ,err :foreground ,war))))

;;;;; other, need more work
            `(ac-completion-face ((,class (:underline t :foreground ,keyword))))
            `(ffap ((,class (:foreground ,base))))
            `(flx-highlight-face ((,class (:foreground ,comp :underline nil))))
            `(icompletep-determined ((,class :foreground ,keyword)))
            `(js2-external-variable ((,class (:foreground ,comp))))
            `(js2-function-param ((,class (:foreground ,const))))
            `(js2-jsdoc-html-tag-delimiter ((,class (:foreground ,str))))
            `(js2-jsdoc-html-tag-name ((,class (:foreground ,keyword))))
            `(js2-jsdoc-value ((,class (:foreground ,str))))
            `(js2-private-function-call ((,class (:foreground ,const))))
            `(js2-private-member ((,class (:foreground ,base))))
            `(js3-error-face ((,class (:underline ,war))))
            `(js3-external-variable-face ((,class (:foreground ,var))))
            `(js3-function-param-face ((,class (:foreground ,keyword))))
            `(js3-instance-member-face ((,class (:foreground ,const))))
            `(js3-jsdoc-tag-face ((,class (:foreground ,keyword))))
            `(js3-warning-face ((,class (:underline ,keyword))))
            `(slime-repl-inputed-output-face ((,class (:foreground ,comp))))
            `(trailing-whitespace ((,class :foreground nil :background ,err)))
            `(undo-tree-visualizer-current-face ((,class :foreground ,keyword)))
            `(undo-tree-visualizer-default-face ((,class :foreground ,base)))
            `(undo-tree-visualizer-register-face ((,class :foreground ,comp)))
            `(undo-tree-visualizer-unmodified-face ((,class :foreground ,var))))

           (custom-theme-set-variables
            theme-name
            `(ansi-color-names-vector [,bg4 ,red ,green ,yellow ,blue ,magenta ,cyan ,base]))

           ))


;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide 'pure-common)

;; Local Variables:
;; no-byte-compile: t
;; End:

;;; pure-common.el ends here
